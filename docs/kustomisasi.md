# Kustomisasi

## Menambahkan package

Anda dapat menambahkan *package* sesuai kebutuhan anda pada bagian
"Add your own needed package here"

## Merubah *font*

"Font style"

## Merubah gaya sitasi

"Bibliography style"

## Merubah margin dokument

"Page layout" pada berkas kelas

## Merubah tata letak sampul

"Define cover in Indonesian" pada berkas kelas

